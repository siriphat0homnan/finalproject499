var Product = require('../models/product');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/shopping', { useNewUrlParser: true });

var products = [
    new Product({
        type: 'Nature',
        imagePath: 'images/portfolio-img1.jpg',
        address: '0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B',
        price: 0.00025
    }),
    new Product({
        type: 'Nature',
        imagePath: 'images/portfolio-img1.jpg',
        address: '0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B',
        price: 0.00025
    }),
    new Product({
        type: 'Nature',
        imagePath: 'images/portfolio-img1.jpg',
        address: '0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B',
        price: 0.00025
    }),
];

var done = 0;

for(var i = 0; i < products.length; i++){
    products[i].save(function (err, result) {
        done++;
        if(done === products.length){
            exit();
        }
    });
}

function exit(){
    mongoose.disconnect();
}

