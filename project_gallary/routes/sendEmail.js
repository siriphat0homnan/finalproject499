var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');

router.post('/', function(req, res, next) {
    var output = `
    <p>YOU have a new contact request</p>
    <h3> Contact Details</h3>
    <ul>
    <li>Email: ${req.body.email}</li>
    <li>Transaction: ${req.body.transaction}</li>
    <li>ETH: 0.5</li>
    <li>TIM: 50</li>
    <li>message: ${req.body.message}</li>
</ul>
    `;
    var id = req.body.email;
    var pass = req.body.password;

    const transporter = nodemailer.createTransport({
        service: 'hotmail',
        auth: {
            user:  id, // your email
            pass: pass // your email password
        }
    });

    var mailOptions = {
        from: id,
        to: 'siriphat_homnan@hotmail.com',
        subject: 'Sending Email',
        text: 'Ok',
        html: output
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
            res.render('receiveTimToken');
        } else {
            console.log('Email sent: ' + info.response);
            res.render('send', {msh: 'EMAIL SENDING WAIT TO RECEIVE TIM TOKENS.'});
        }
    });

});

module.exports = router;
// <li>ETH: ${req.body.ethShow}</li>
// <li>TIM: ${req.body.timShow}</li>