var express = require('express');
var router = express.Router();
var Product = require('../models/product');
var ObjectId = require('mongodb').ObjectID;


router.get('/:id', function(req, res, next) {
    Product.find({'_id' : ObjectId(req.params.id)}, function (err, docs) {
        res.render('profile', { title: 'Profile', id: docs[0] });
        // res.send(docs[0]);
    });
});

module.exports = router;
