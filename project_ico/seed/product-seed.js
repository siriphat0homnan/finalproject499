var Product = require('../models/product');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/Fashion', { useNewUrlParser: true });

var products = [
    new Product({
        type: 'woman',
        imagePath: 'img/product-img/product-1.jpg',
        name: 'A',
        detail: 'A',
        address: '0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B',
        price: 0.005
    }),
    new Product({
        type: 'man',
        imagePath: 'img/product-img/product-2.jpg',
        name: 'B',
        detail: 'B',
        address: '0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B',
        price: 0.0025
    }),
    new Product({
        type: 'access',
        imagePath: 'img/product-img/product-3.jpg',
        name: 'C',
        detail: 'C',
        address: '0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B',
        price: 0.0025
    }),
    new Product({
        type: 'shoes',
        imagePath: 'img/product-img/product-4.jpg',
        name: 'D',
        detail: 'D',
        address: '0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B',
        price: 0.0035
    }),
    new Product({
        type: 'kids',
        imagePath: 'img/product-img/product-5.jpg',
        name: 'E',
        detail: 'E',
        address: '0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B',
        price: 0.0045
    }),
    new Product({
        type: 'kids',
        imagePath: 'img/product-img/product-6.jpg',
        name: 'F',
        detail: 'F',
        address: '0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B',
        price: 0.0055
    }),
];

var done = 0;

for(var i = 0; i < products.length; i++){
    products[i].save(function (err, result) {
        done++;
        if(done === products.length){
            exit();
        }
    });
}

function exit(){
    mongoose.disconnect();
}

