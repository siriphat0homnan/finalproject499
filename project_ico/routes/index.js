var express = require('express');
var router = express.Router();
var Product = require('../models/product');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'TIM ICO'});
});
//
// /* Post home page. */
// router.post('/', function(req, res, next) {
//     res.render('index', { title: 'TIM ICO'});
// });


module.exports = router;
