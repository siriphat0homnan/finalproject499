var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    type: {type: String, required: true},
    imagePath: {type: String, required: true},
    name: {type: String, required: true},
    detail: {type: String, required: true},
    address: {type: String, required: true},
    price: {type: Number, required: true}
});


module.exports = mongoose.model('Product', schema);

