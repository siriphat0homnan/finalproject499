// const nodemailer = require("nodemailer");
$('#ethBuy').keyup(function () {
    let num = $('#ethBuy').val() * 100;
    $('#timBuy').val(num);
});

$('#timSell').keyup(function () {
    let num = $('#timSell').val() / 100;
    $('#ethSell').val(num);
});

if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
}

console.log(web3);
web3.eth.defaultAccount = web3.eth.accounts[0];
$("#addressSenderBuy").val(web3.eth.accounts[0]);
var ethbuy = $("#ethBuy").val();
$("#addressSenderSell").val(web3.eth.accounts[0]);
var ethsell = $("#ethSell").val();

$("#confirmBuy").click(function() {
    console.log(web3);
    web3.eth.defaultAccount = web3.eth.accounts[0];
    var toAddress = "0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B";
    var eth = $("#ethBuy").val();
    $("#addressSenderBuy").val(web3.eth.accounts[0]);
    web3.eth.sendTransaction({from: web3.eth.accounts[0], to: toAddress, value: web3.toWei(eth, 'ether'), gasLimit: 21000, gasPrice: 20000000000}, function(error, result){
        if(!error){
            $('#ethShow').val($('#ethBuy').val());
            $('#timShow').val($('#timBuy').val());
            $('#type').val($('#buy').val());
            $('#transaction').val(result);
            // console.log($('#ethBuy').val(), $('#timBuy').val(), result);
            $(document).ready(function(){
                $("#myModalEmail").modal('show');
            });
            console.log(JSON.stringify(result));
        }else{
            console.error(error);
        }
    });
});

$("#confirmSell").click(function() {
    web3.eth.defaultAccount=web3.eth.accounts[0];
    var timtokenContract = web3.eth.contract([{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"spender","type":"address"},{"name":"tokens","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"from","type":"address"},{"name":"to","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"_totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"tokenOwner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"acceptOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"a","type":"uint256"},{"name":"b","type":"uint256"}],"name":"safeSub","outputs":[{"name":"c","type":"uint256"}],"payable":false,"stateMutability":"pure","type":"function"},{"constant":false,"inputs":[{"name":"to","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"a","type":"uint256"},{"name":"b","type":"uint256"}],"name":"safeDiv","outputs":[{"name":"c","type":"uint256"}],"payable":false,"stateMutability":"pure","type":"function"},{"constant":false,"inputs":[{"name":"spender","type":"address"},{"name":"tokens","type":"uint256"},{"name":"data","type":"bytes"}],"name":"approveAndCall","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"a","type":"uint256"},{"name":"b","type":"uint256"}],"name":"safeMul","outputs":[{"name":"c","type":"uint256"}],"payable":false,"stateMutability":"pure","type":"function"},{"constant":true,"inputs":[],"name":"newOwner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"tokenAddress","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transferAnyERC20Token","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"tokenOwner","type":"address"},{"name":"spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"a","type":"uint256"},{"name":"b","type":"uint256"}],"name":"safeAdd","outputs":[{"name":"c","type":"uint256"}],"payable":false,"stateMutability":"pure","type":"function"},{"constant":false,"inputs":[{"name":"_newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"tokens","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"tokenOwner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"tokens","type":"uint256"}],"name":"Approval","type":"event"}]);
    var token = timtokenContract.at('0x3c963a1759c5af9f63e42e2b5c2a409961c6c65c');
    console.log(token);
    let toAddress = "0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B";
    let uint256Id = ((10) ** 18) * $("#timSell").val();
    token.transfer(toAddress, uint256Id, (err, res) => {
        if (err){
            console.log("Nooo" + err);
        }else{
            console.log("Success" + res);
            $('#ethShow').val($('#ethSell').val());
            $('#timShow').val($('#timSell').val());
            $('#type').val($('#sell').val());
            $('#transaction').val(res);
            $(document).ready(function(){
                $("#myModalEmail").modal('show');
            });
        }
    });

    // console.log(web3);
    // web3.eth.defaultAccount = web3.eth.accounts[0];
    // var toAddress = "0x88D1c3eb41ECADdb6dd8797F1661Bdd2eC31047B";
    // var eth = $("#ethSell").val();
    // $("#addressSenderSell").val(web3.eth.accounts[0]);
    // web3.eth.sendTransaction({from: web3.eth.accounts[0], to: toAddress, value: web3.toWei(eth, 'ether'), gasLimit: 21000, gasPrice: 20000000000}, function(error, result){
    //     if(!error){
    //         $('#ethShow').val($('#ethSell').val());
    //         $('#timShow').val($('#timSell').val());
    //         $('#type').val($('#sell').val());
    //         $('#transaction').val(result);
    //         $(document).ready(function(){
    //             $("#myModalEmail").modal('show');
    //         });
    //
    //         console.log(JSON.stringify(result));
    //     }else{
    //         console.error(error);
    //     }
    // });
});

$("#sendEmail").click(function() {
    $('#myModalEmail').modal('hide');
    $('#myModal2').modal('hide');
    $('#myModal1').modal('hide');
    $("#waitAdmin").modal('show');
});




