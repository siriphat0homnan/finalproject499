var express = require('express');
var router = express.Router();
var Product = require('../models/product');

/* GET home page. */
router.get('/', function(req, res, next) {
  Product.find(function (err, docs) {
      var productLine = [];
      var line = 3;
      var i;
      for (i = 0; i < docs.length; i += line){
          productLine.push(docs.slice(i, i + line));
      }
      res.render('index', { title: 'Fashion Shop', products: productLine });
  });
});

module.exports = router;
