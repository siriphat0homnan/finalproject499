var express = require('express');
var router = express.Router();

/* GET contractpage. */
router.get('/', function(req, res, next) {
  res.render('contract', { title: 'Fashion Shop' });
});

module.exports = router;
